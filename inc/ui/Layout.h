#ifndef LAYOUT_H
#define LAYOUT_H

#include <functional>
#include <utility>
#include <memory>
#include <algorithm>
#include <vector>
#include <string>
#include <cstring>
#include "bus/IFunction.h"
#include "Obj.h"
#include "bus/BusSlave.h"

class UI;
class Obj;
class IFunction;

struct UI_FLAGS{
	bool redraw, 
	hidden,
	collapsed,
	wrapText,
	drawBorders,
	focused,
	readyForDeletion;
};

class Layout : public BusSlave {
	public:
		Layout(const Layout& layout);
		Layout(ID<UI> _id = ID<UI>(), int _w = 0, int _h = 0, int _x = 0, int _y = 0, 
				std::string name = "Unamed Layout");
		~Layout();

		template<class T> void addObj(std::shared_ptr<T> obj);
		template<class T> std::shared_ptr<T> getObj(int id);

		void removeObj(int id);
		void mvObj(int x, int y, int id);
		void mvObjFocus(int direction);
		void update();

		ID<UI> getObjID();
		int getHeight();
		int getWidth();
		int getYO();
		int getXO();
		int getSelectedIndex();
		std::shared_ptr<Obj> getFocused();
		std::vector<std::shared_ptr<Obj>> getVector();
		std::vector<std::shared_ptr<Obj>> getDrawData();
		std::vector<IFunction> getIFunctions() override;

		bool isReadyToDelete();
		bool wrapsText();
		bool hasBorders();
		bool needsRedraw();
		bool isFocused();
		bool isCollapsed();
		bool isHidden();

		void setHeight(int h);
		void setWidth(int w);
		void setPosOffset(int x, int y);
		void setSelectedIndex(int id);

		void setRedraw(bool state);
		void setWrapText(bool state);
		void setDrawBorders(bool state);
		void setFocus(bool state);
		void setCollapsed(bool state);
		void setHidden(bool state);

		void readyToDelete();
		void switchCollapseState();
		void switchHideState();

	private:
		std::vector<std::shared_ptr<Obj>> objVec;
		std::vector<std::shared_ptr<Obj>> focusable;
		std::vector<std::shared_ptr<Obj>>::iterator focused;

		int height;
		int width;
		int xOffset;
		int yOffset;

		ID<UI> id; 				//actual layout id

		UI_FLAGS lFlags;
};

//template definitions

template<class T>
void Layout::addObj(std::shared_ptr<T> _obj){
	try{
		std::shared_ptr<Obj> inObj = std::static_pointer_cast<Obj>(_obj);
		inObj->setPosOffset(xOffset, yOffset);

		if(inObj->isFocusable()){
			focusable.push_back(inObj);
			focused = focusable.begin();
		}
		
		Logger::o << DECO << "Adding obj" << inObj->getObjID().getID() << " to layout" << id.getID() << ". Data is '" << inObj->getData() << "'.\n";
		objVec.push_back(std::move(inObj));
	}catch(const std::bad_cast& ex){
		 Logger::o << DECO << ex.what() << ".\n";
	}
}

template<class T>
std::shared_ptr<T> Layout::getObj(int _id){
	for(auto it = objVec.begin(); it != objVec.end(); ++it){
		if((*it)->getObjID().getID() == _id){
			std::shared_ptr<T> obj = std::dynamic_pointer_cast<T>((*it));
			
			if(obj){
				return obj;
			}else{
				Logger::slog("Failed to cast obj. Program will now probably break...");
				return nullptr;
			}
		}
	}
}

#endif
