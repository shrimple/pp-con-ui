#ifndef ID_H
#define ID_H

#include <string>

class Obj;
class Layout;

template<class PARENT>
class ID {
	private:
		PARENT* parent;
		int id;

	public:
		int getID();
		PARENT* getParent();
		void setParent(int id, PARENT* layPtr);
		bool operator==(ID const& rhs);
		bool operator!=(ID const& rhs);

		ID(const ID<PARENT>& obj);
		ID(PARENT* _p = nullptr, int _id = -1):parent{_p},id{_id}{};

		~ID();
};

template<class PARENT>
ID<PARENT>::ID(const ID& _obj){
	parent = _obj.parent;
	id = _obj.id;
}

template<class PARENT>
bool ID<PARENT>::operator==(ID const& rhs){
	return (id == rhs.id);
}

template<class PARENT>
bool ID<PARENT>::operator!=(ID const& rhs){
	return (id != rhs.id);
}

template<class PARENT>
int ID<PARENT>::getID(){
	return id;
}

template<class PARENT>
PARENT* ID<PARENT>::getParent(){
	return parent;
}

template<class PARENT>
void ID<PARENT>::setParent(int _id, PARENT* _p){
	parent = _p;
	id = _id;
}

//maybe do selfcheck
template<class PARENT>
ID<PARENT>::~ID(){
	
}

#endif
