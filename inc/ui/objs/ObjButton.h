#ifndef OBJBUTTON_H
#define OBJBUTTON_H

#include <pdcurses/curses.h>
#include <string>
#include <vector>
#include "Generics.h"
#include "Logger.h"
#include "Obj.h"
#include "bus/IFunction.h"
#include "bus/BusOperator.h"

class Bus;

class ObjButton : public Obj, public BusOperator {
	private:

	public:
		std::string getData() override;

		ObjButton(const ObjButton& obj);
		ObjButton(ID<Layout> id = ID<Layout>(), int xOff = 0, int yOff = 0, int color = 0, int selColor = 1,
				  std::string data = "Unlabeled", std::shared_ptr<IFunction> f = nullptr, STR_ID name = "Unamed");
		~ObjButton();
};
#endif
