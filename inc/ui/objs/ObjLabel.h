#ifndef OBJLABEL_H
#define OBJLABEL_H

#include <vector>
#include <string>
#include "bus/BusSlave.h"
#include "bus/IFunction.h"
#include "Obj.h"

class ObjLabel : public Obj, public BusSlave{
    public:
        std::vector<IFunction> getIFunctions() override;
        void changeData(std::string data);
        std::string getData();

        ObjLabel(ID<Layout> id = ID<Layout>(), int xOff = 0, int yOff = 0, int color = 0, int selColor = 1,
				  std::string data = "Unlabeled", STR_ID name = "Unamed Label");
        ~ObjLabel();


};

#endif