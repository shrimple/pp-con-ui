#ifndef BUSSLAVE_H
#define BUSSLAVE_H

#include <vector>
#include <string>
#include "Generics.h"
#include "bus/IFunctionSet.h"
#include "bus/BusClient.h"
#include "Generics.h"

class BusSlave : public BusClient, public std::enable_shared_from_this<BusSlave>{
    public:
        BusSlave(std::vector<IFunction> funcSet = {}, std::string name = "Unamed Slave");
        ~BusSlave();
        
        IFunctionSet getFuncSet();
        bool switchReceiveState();
        void validate(BusRegPayload _f) override;
        virtual std::vector<IFunction> getIFunctions() = 0; 

    private:
        IFunctionSet slaveFunctions;
        bool canReceiveAction;
    
};

#endif