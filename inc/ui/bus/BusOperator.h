#ifndef BUSOPERATOR_H
#define BUSOPERATOR_H

#include <memory>
#include "bus/IFunction.h"
#include "bus/BusClient.h"
#include "Logger.h"

class BusOperator : public BusClient{
    public:
        void doBound();
        void bindTo(std::shared_ptr<IFunction> del);
        void validate(BusRegPayload p) override;
        std::shared_ptr<IFunction> getBoundF();
        
        BusOperator(std::shared_ptr<IFunction> f = nullptr, STR_ID name = "Unamed Operator");
        ~BusOperator();
    private:
        std::shared_ptr<IFunction> boundIFunc;
};

#endif