#ifndef UIBUS_H
#define UIBUS_H

#include <vector>
#include <functional>
#include <string>
#include <memory>
#include "Generics.h"
#include "bus/BusClient.h"
#include "bus/BusOperator.h"
#include "bus/BusSlave.h"
#include "Logger.h"
#include "bus/IFunctionSet.h"

using bs_shp = std::shared_ptr<BusSlave>;

class Bus {
	public:
		void registerOperator(BusOperator* client);
		void registerSlave(bs_shp client);

		void bindToOperator(std::shared_ptr<BusOperator> op, STR_ID name, FunctionTag tag);
		void bindToOperator(std::shared_ptr<BusOperator> op, int cid, FunctionTag tag);

		int getNewClientID();
		int getNewActionID();

		Bus();
		~Bus();

	private:
		int clientGen;
		int iFuncGen;
		std::vector<std::shared_ptr<BusSlave>> registeredClients;
};

#endif
