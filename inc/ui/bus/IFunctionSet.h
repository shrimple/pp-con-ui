#ifndef IFUNCTIONSET_H
#define IFUNCTIONSET_H

#include <memory>
#include <vector>
#include "Generics.h"
#include "bus/IFunction.h"
#include "Logger.h"

class IFunctionSet{
    public:
    IFunctionSet(std::vector<IFunction> a = {});
    ~IFunctionSet();

    std::shared_ptr<IFunction> get(int i);
    std::shared_ptr<IFunction> getByID(int i);
    std::shared_ptr<IFunction> getByTag(FunctionTag t);

    std::vector<std::shared_ptr<IFunction>> getVector();
    int size();

    private:
    std::vector<std::shared_ptr<IFunction>> actions;

};

#endif