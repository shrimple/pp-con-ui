#ifndef IFUNCTION_H
#define IFUNCTION_H
#include <functional>
#include <string>
#include "ID.h"
#include "Logger.h"
#include "Generics.h"

class IFunction{
	public:
		FunctionTag getTag();
		int getID();
		virtual bool doFunction();

		void validate(int id);

		IFunction(std::function<void()> f = nullptr, FunctionTag tag = NOTHING);
		~IFunction();

	private:
		FunctionTag tag;
		int actionID;
		std::function<void()> action;
};

#endif
