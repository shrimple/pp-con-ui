#ifndef BUSCLIENT_H
#define BUSCLIENT_H

#include <string>
#include <memory>
#include "bus/IFunctionSet.h"
#include "Generics.h"
#include "Logger.h"

class Bus;

class BusClient{
    friend Bus;

    public:
        BusClient(const BusClient& client);
        BusClient(std::string n = "Unamed");
        ~BusClient();

        std::string getName();
        int getID();
        virtual void validate(BusRegPayload _token) = 0;

    protected:
        STR_ID name;
        int busClientId;
        Bus* busPtr;
        bool valid;

        void registerMe(Bus* bus, int clId);
};

#endif