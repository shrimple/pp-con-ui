#ifndef OBJ_H
#define OBJ_H

#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <pdcurses/curses.h>
#include "Generics.h"
#include "Logger.h"
#include "ID.h"

class Obj {
	public:		
		ID<Layout> getObjID();
		virtual std::string getData() = 0;
		int getColor();
		int getXO();
		int getYO();
		
		int getFocusColor();
		bool isFocusable();
		bool isFocused();

		void setColor(int color);	
		void setPosOffset(int xOff, int yOff);
		void setFocusColor(int color);
		void setFocusable(bool state);
		void setFocus(bool state);
		
		Obj(const Obj& obj);
		Obj(ID<Layout> _id = ID<Layout>(), int _xOff = 0, int _yOff = 0, int _color = 0, int _fColor = 1, bool _focusable = false, std::string _data = "No data");
		~Obj();

	protected:
		ID<Layout> id;
		std::string data;

		int xOffset;
		int yOffset;
		int color;
		int focusedColor;
		bool focusable;
		bool focused;
};

#endif
