#ifndef UI_H
#define UI_H

#include <functional>
#include <vector>
#include <algorithm>
#include <string>
#include <iterator>
#include <cstdio>
#include <cstring>
#include <pdcurses/curses.h>
#include "Generics.h"
#include "Logger.h"
#include "ObjFactory.h"
#include "Layout.h"
#include "bus/IFunction.h"
#include "bus/Bus.h"


class UI {
	private:
		Bus bus;

		std::vector<std::shared_ptr<Layout>> layoutVec;
		std::vector<std::shared_ptr<Layout>>::iterator focusedLayout;
		bool readyToClose;
		bool showConsole;
		bool requireRedraw;

		void initColorPairs();
		void initCurses();

	public:
		void catchInput();
		void exitUI();
		void drawBorders(int h, int w, int x, int y);
		std::string borderStr(int w, std::string border, char fill);
		void draw();
		bool checkForRedraw();
		void printLayout(std::shared_ptr<Layout> l);
		void switchLoggerViewState();
		void update(bool force);

		BusRegPayload getRegPayload();

		std::shared_ptr<Layout> getLayout(int id);
		std::shared_ptr<Layout> loadLayout(std::shared_ptr<Layout> l, bool _f);
		std::shared_ptr<Layout> loadLayout(Layout l, bool _f);
		void removeLayout(int index);
		Bus* getBus();

		bool isReadyToClose();

		void moveFocus(int _dir);

		UI();
		~UI();
};
#endif
