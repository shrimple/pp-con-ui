#ifndef OBJFACTORY_H
#define OBJFACTORY_H

#include <memory>
#include <string>
#include "Generics.h"
#include "Logger.h"
#include "ID.h"
#include "Layout.h"
#include "objs/ObjButton.h"
#include "objs/ObjLabel.h"
#include "bus/IFunction.h"

class UI;

    class OFac{
        private:
            static int objGen;
            template<class P> static ID<P> getNewID(P* parent);

        public:
            static std::shared_ptr<Layout> makeLayout(UI* p, int w, int h, int x, int y, BusRegPayload payload, STR_ID name);
            static std::shared_ptr<ObjLabel> makeLabel(Layout* p, int x, int y, std::string label, int color, BusRegPayload payload, STR_ID name);
            static std::shared_ptr<ObjButton> makeButton(Layout* p, int x, int y, std::string label, int color, int selColor, BusRegPayload payload, STR_ID name);
            ~OFac();
    };


    template<class P>
    ID<P> OFac::getNewID(P* _p){
        ID<P> id = ID<P>(_p, objGen++);
        return id;
    }
#endif