#ifndef TIMR_H
#define TIMR_H
#include <chrono>

struct TIMER_FLAGS{
    bool pause, done, looping;
};

class Timr{
    public:
        Timr(int length, bool bornOn, bool loop);
        ~Timr();

        void start();
        void pause();
        void stop();
        void reset(bool bornOn);

        bool isPaused();
        bool isDone();

        int now();

    private:
        std::chrono::time_point<std::chrono::steady_clock> init, end, now;
        std::chrono::duration<double> length;
        TIMER_FLAGS tFlags;

};

#endif