#ifndef LOGGER_H
#define LOGGER_H

#include <pdcurses/curses.h>
#include <functional>
#include <memory>
#include <type_traits>
#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <thread>
#include "Generics.h"

struct LoggerOutput{
  LoggerOutput():printFunc{nullptr}, length{100}, streamedStr{""}{};
  LoggerOutput(std::function<void(std::string)> _f, int _l){streamedStr = ""; length = _l; printFunc = _f;}

  LoggerOutput& operator<<(std::string s){ print(s); return *this;}
  LoggerOutput& operator<<(char c){ print(std::to_string(c)); return *this;}
  LoggerOutput& operator<<(int i){ print(std::to_string(i)); return *this;}
  //LoggerOutput& operator<<(bool b){ if(b)print("true");else print("false"); return *this;}
  LoggerOutput& operator<<(long l){ print(std::to_string(l)); return *this;}
  LoggerOutput& operator<<(double d) {print(std::to_string(d)); return *this;}
  LoggerOutput& operator<<(std::size_t i){ print(std::to_string(i)); return *this;}

  private:
    std::string streamedStr;
    bool gotCallerName;

    void print(std::string _str){
      streamedStr.append(_str);

      if(streamedStr.find("\n") != std::string::npos){
        std::cout << streamedStr;
        printFunc(streamedStr);
        streamedStr = "";
      }
    };
    std::function<void(std::string)> printFunc;
    int length;
};

class Logger {
  private:
    static std::vector<std::string> logHistory;
    static bool active;
    static bool isInit;
    static int width;
    static int height;
    static int cursorPos;
    static Position pos;
    static std::string completeStr(std::string str);

  public:
    static bool needDrawing;
    static LoggerOutput o;

    static void drawLogger();
    static std::vector<std::string> getViewableVector();
    static void moveViewPort(int amt);
    static void getVector();
    static void slp(int ms);
    static void init(int w, int h, Position p);
    static void slog(std::string str);
    static void log(std::string str);
    static void setActive(bool active);
    static bool isActive();
};

#endif
