#ifndef GENERICS_H
#define GENERICS_H
#include <memory>
#include <string.h>
#include <functional>

class BusOperator;
class BusSlave;

// <font color>_<font bg color>
#define WHITE_BLACK 1
#define RED_BLACK 2
#define GREEN_BLACK 3
#define YELLOW_BLACK 4
#define BLUE_YELLOW 5
#define MAGENTA_BLACK 6
#define CYAN_BLACK 7
#define BLACK_WHITE 8

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define DECO "[" << __FILENAME__ << "] <" << __FUNCTION__ << "> "

typedef std::function<void(BusOperator*)> BUS_REG;
typedef std::function<void(std::shared_ptr<BusSlave>)> SLAVE_REG;
typedef std::string STR_ID;

enum FunctionTag{
	//generic for all objects
	NOTHING, HIDE, KILL, 
	//specific to layout 
	LAYOUT_OPEN, LAYOUT_COLLAPSE, LAYOUT_CHANGE_POS, LAYOUT_RESIZE,
	//specific to objects and its derivatives
	OBJ_CHANGE_TEXT, OBJ_CHANGE_POS
};

struct Position{
    int x, y;
    Position(int x = 0, int y = 0):x{x},y{y}{};
};

struct BusRegPayload{
    BusRegPayload(SLAVE_REG _sR = nullptr, BUS_REG _cR = nullptr):clientReg{_cR}, slaveReg{_sR}{};
    const BUS_REG clientReg;
    const SLAVE_REG slaveReg;
};

static std::string tag2str(FunctionTag t){
	switch(t){
		case NOTHING: return "Nothing";
		case HIDE: return "Hide";
		case KILL: return "Kill";
		case LAYOUT_OPEN: return "Layout Open";
        case LAYOUT_COLLAPSE: return "Layout Collapse";
        case LAYOUT_CHANGE_POS: return "Layout Change Pos";
        case LAYOUT_RESIZE: return "Layout Resize";
        case OBJ_CHANGE_POS: return "Layout Change Pos";
        case OBJ_CHANGE_TEXT: return "Layout Change Text";
	}

    return "Undefined tag";
}

static std::string b2str(bool b){
	if(b) return "true"; else return "false";
}

#endif
