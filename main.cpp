#include <vector>
#include <iostream>
#include <pdcurses/curses.h>
#include "UI.h" 
#include "Layout.h"
#include "ObjFactory.h"
#include "bus/IFunctionSet.h"
#include "Generics.h"

void main_menu(UI& ui){
	std::shared_ptr<Layout> layout = ui.loadLayout(OFac::makeLayout(&ui, 23, 12, 1, 1, ui.getRegPayload(), "Lay1"), true);
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 1, 1, "++ Console UI v0.9", BLACK_WHITE,  ui.getRegPayload(), "Label1"));
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 2, 2, "built by shrimple", BLACK_WHITE,  ui.getRegPayload(), "Label2"));
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 1, 4, ":controls:", BLUE_YELLOW,  ui.getRegPayload(), "Label3"));
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 1, 5, "x-previous layout", CYAN_BLACK,  ui.getRegPayload(), "Label4"));
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 1, 6, "c-next layout", CYAN_BLACK,  ui.getRegPayload(), "Label5"));
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 1, 7, "f-interact", CYAN_BLACK,  ui.getRegPayload(), "Label6"));
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 1, 8, "e*2-exit", CYAN_BLACK,  ui.getRegPayload(), "Label7"));
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 1, 9, "w/s-change button", CYAN_BLACK,  ui.getRegPayload(), "Label8"));
	layout->addObj<ObjLabel>(OFac::makeLabel(layout.get(), 1, 10, "\\-open logger", CYAN_BLACK,  ui.getRegPayload(), "Label9"));

	std::shared_ptr<ObjButton> b1 = OFac::makeButton(layout.get(), 3, 0, "-", GREEN_BLACK, BLACK_WHITE , ui.getRegPayload(), "Button1");
	ui.getBus()->bindToOperator(b1, "Lay1", FunctionTag::LAYOUT_COLLAPSE);
	layout->addObj<ObjButton>(b1);

	std::shared_ptr<ObjButton> b2 = OFac::makeButton(layout.get(), 1, 0, "X", MAGENTA_BLACK, BLACK_WHITE, ui.getRegPayload(), "Button2");
	ui.getBus()->bindToOperator(b2, "Lay1", FunctionTag::KILL);
	layout->addObj<ObjButton>(b2);

	layout->setDrawBorders(true);
}

void side_menu(UI& ui){
	std::shared_ptr<Layout> layout = ui.loadLayout(OFac::makeLayout(&ui, 20, 10, 10, 3, ui.getRegPayload(), "Lay2"), true);
	layout->addObj<Obj>(OFac::makeLabel(layout.get(), 1, 2, "Crazy how time", BLACK_WHITE, ui.getRegPayload(), "Label10"));
	layout->addObj<Obj>(OFac::makeLabel(layout.get(), 1, 3, "passes...", BLACK_WHITE, ui.getRegPayload(), "Label11"));

	std::shared_ptr<ObjButton> b1 = OFac::makeButton(layout.get(), 1, 8, "HIDE", WHITE_BLACK, BLACK_WHITE , ui.getRegPayload(), "Button3");
	ui.getBus()->bindToOperator(b1, "Lay2", FunctionTag::LAYOUT_COLLAPSE);
	layout->addObj<ObjButton>(b1);

	std::shared_ptr<ObjButton> b2 = OFac::makeButton(layout.get(), 1, 9, "KILL", CYAN_BLACK, BLACK_WHITE, ui.getRegPayload(), "Button4");
	ui.getBus()->bindToOperator(b2, "Lay2", FunctionTag::KILL);
	layout->addObj<ObjButton>(b2);

	layout->setDrawBorders(true);
}

void xtra_menu(UI& ui){
	std::shared_ptr<Layout> layout = ui.loadLayout(OFac::makeLayout(&ui, 20, 10, 15, 6, ui.getRegPayload(), "Lay3"), true);
	layout->addObj<Obj>(OFac::makeLabel(layout.get(), 1, 2, "one more", BLACK_WHITE, ui.getRegPayload(), "Label12"));
	layout->addObj<Obj>(OFac::makeLabel(layout.get(), 1, 3, "for the road...", BLACK_WHITE, ui.getRegPayload(), "Label13"));


	layout->setDrawBorders(true);
}


int main(){  
	initscr();

	if(has_colors() == FALSE){
		endwin();
		printf("NO COLOR SUPPORT, NO APP 4 U. >:)");
		exit(1);
	}

	start_color();
	UI ui = UI();
	main_menu(ui);
	side_menu(ui);
	xtra_menu(ui);
	
	while(!ui.isReadyToClose()){
		ui.update(false);
	}

	return 1;
}
