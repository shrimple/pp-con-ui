CC = g++ -std=c++2a
CFLAGS = -g -Wall -o
LTARGET = itemgen.exe

VPATH = src/ui src/ui/bus src/ui/objs src/util \
	inc/ui inc/ui/bus inc/ui/objs inc/util

vpath %.cpp src/ui src/ui/bus src/ui/objs src/util 
vpath $.h inc/ui inc/ui/bus inc/ui/objs inc/util

OBJS = main.o \
       UI.o Obj.o Layout.o ObjButton.o ObjLabel.o  ObjFactory.o \
       Bus.o IFunction.o BusClient.o BusSlave.o BusOperator.o IFunctionSet.o \
       Logger.o

REBUILDABLES = $(OBJS) $(LTARGET) .deps
HINC =  -Iinc/ui -Iinc/util -IC:/msys64/mingw64/include/ -IC:/msys64/mingw64/include/pdcurses/   
LDFLAGS = -LC:/msys2/msys64/mingw64/lib -lpdcurses -lstdc++ -static

all: $(LTARGET)
	echo Building done

clean:
	rm -f $(REBUILDABLES)
	echo Removed rebuildables

$(LTARGET) : $(OBJS)
	$(CC) $(CFLAGS) $@ $^ $(HINC) $(LDFLAGS)

%.o : %.cpp
	$(CC) $(CFLAGS) $@ -c $< $(HINC) $(LDFLAGS)

%.o %d: %.cpp
	$(CC) -c $< $(HINC) $(LDFLAGS) -MMD -MP -MF $*.d

include $(wildcard *.d)
