#include "Logger.h"

std::vector<std::string> Logger::logHistory;
bool Logger::needDrawing = false;
bool Logger::active = true;
bool Logger::isInit = false;
int Logger::width = 100;
int Logger::height = 20;
LoggerOutput Logger::o;
Position Logger::pos = Position(0, 0);
int Logger::cursorPos = 0;


std::vector<std::string> Logger::getViewableVector(){
	if(logHistory.size() > cursorPos + height )
		return std::vector<std::string>(logHistory.begin() + cursorPos , logHistory.begin() + cursorPos + height);
	else
		return std::vector<std::string>(logHistory.begin() + cursorPos , logHistory.end());
}

void Logger::moveViewPort(int _amt){
	switch(_amt){
		case -1: if( cursorPos > 0 ) cursorPos--; 
		break;
		case 1: 
		if(cursorPos + _amt + height > logHistory.size()) 
			cursorPos = logHistory.size() - height; 
		else
			cursorPos++;				
		break;
	}

	needDrawing = true;
}

void Logger::init(int _w, int _h, Position _pos){
	width = _w;
	height = _h;
	pos = _pos;
	isInit = true;
	o = LoggerOutput(log, width);
	o << DECO << "Logger initialized...\n";
}

void Logger::drawLogger(){
	int i = 1;
	std::string border = std::string((width/2)-3, '+') + " LOGGER " + std::string((width/2)-4, '+');
	mvprintw(0, 0, border.c_str());

	std::vector<std::string> v = getViewableVector();

	//o << DECO << v.size() << " objs to draw. logHistory: " << logHistory.size() << ". curpos:" << cursorPos << ".\n";

	for(auto it = v.begin(); it != v.end(); it++){
		mvprintw(i, 0, it->c_str());
		i++;	
	}

	border = std::string(width, '+');
	mvprintw(height, 0, border.c_str());

	needDrawing = false;
}

void Logger::slog(std::string _msg){
	log(_msg);
	slp(2000);
}

void Logger::log(std::string _msg){
	if(!isInit){
		mvprintw(0, 0, "Logger not initialized!");
		return;
	}

	if(isActive){
		int i = 1;
		
		if(_msg.size() <= width)
			logHistory.push_back(completeStr(_msg));
		else{
			while(_msg.size() > width*i){
				std::string newStr = completeStr(_msg.substr(width*(i-1), width));
				logHistory.push_back(newStr);
				i++;
			}
		}
	}else{
		logHistory.push_back("Logger is not active.");
		return;
	}

	needDrawing = true;
}

std::string Logger::completeStr(std::string _str){
	std::string newStr = "+ " + _str;
	newStr += std::string(width - _str.size() - 1, ' ') + "+" ;
	return _str;
}

void Logger::slp(int _ms){
	std::this_thread::sleep_for(std::chrono::milliseconds(_ms));
}


void Logger::setActive(bool _active){
	active = _active;
}

bool Logger::isActive(){
	return active;
}
