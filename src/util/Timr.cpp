#include "Timr.h"

Timr::Timr(double _length, bool _bornOn, bool _loop){
    init = std::chrono::steady_clock::now();
    length = std::chrono::duration_cast<std::chrono::milliseconds>(_length);

    tFlags.looping = true;
    tFlags.done = false;
    

    if(_bornOn)
        start();
    else
        tFlags.pause = true;
    
}


Timr::~Timr(){
    
}


bool Timr::isDone(){
    now = std::chrono::steady_clock::now();
    tFlags.done = (now > end && !tFlags.looping) ? true : false;

    if(tFlags.looping) 
        start();
        
    return tFlags.done;
}


bool Timr::isPaused(){
    return tFlags.pause;
}


void Timr::start(){
    init = now = std::chrono::steady_clock::now();

    if(!tFlags.pause)
        end = now + length;
    else{
        end = now + (length - (now - start));
        tFlags.pause = false;
    }
}


void Timr::pause(){
    tFlags.pause = true;
}


void Timr::stop(){
    tFlags.done = true;
}


void Timr::reset(bool _bornOn){
    tFlags.done = false;
    tFlags.pause = false;

    if(_bornOn)
        start();
}