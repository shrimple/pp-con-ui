#include "bus/BusOperator.h"

BusOperator::BusOperator(std::shared_ptr<IFunction> _f, STR_ID _name):BusClient(_name),boundIFunc{_f}{}
BusOperator::~BusOperator(){}

std::shared_ptr<IFunction> BusOperator::getBoundF(){
    return boundIFunc;
}

void BusOperator::doBound(){
    if(boundIFunc != nullptr){
        Logger::o << DECO << "Calling '" << tag2str(boundIFunc->getTag()) << "' bound func\n";
        boundIFunc->doFunction();
    }
    return;
}

void BusOperator::validate(BusRegPayload _p){
    if(_p.clientReg != nullptr){
        _p.clientReg(this);
    }else if(_p.clientReg == nullptr && busPtr == nullptr){
        Logger::o << DECO << "Client was not initialized via the bus. You must provide a valid reference to the bus validation cb!\n";
    }else if(_p.clientReg == nullptr && busPtr != nullptr){
        Logger::o << DECO << "Client" << busClientId << " already initialized.\n";
    }
}

void BusOperator::bindTo(std::shared_ptr<IFunction> _del){
    boundIFunc = _del;
}