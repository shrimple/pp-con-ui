#include "bus/BusClient.h"

BusClient::BusClient(const BusClient& _client):busClientId{_client.busClientId}, busPtr{_client.busPtr}, valid{_client.valid}, name{_client.name}{}

BusClient::BusClient(STR_ID _name):busClientId{-1}, busPtr{nullptr}, valid{false}, name{_name}{
    Logger::o << DECO << "BusClient " << name <<" constructed!\n";
}

BusClient::~BusClient(){}

int BusClient::getID(){
    return busClientId;
}

std::string BusClient::getName(){
    return name;
}

void BusClient::registerMe(Bus* _bus, int _clId){
    busPtr = _bus;
    busClientId = _clId;
    valid = true;

    if(name.find("Unamed") != std::string::npos){
        name.append(std::to_string(busClientId));
    }

    Logger::o << DECO << "BusClient" << name << "-" << busClientId <<" validated!\n";
}

