#include "bus/IFunction.h"

IFunction::IFunction(std::function<void()> _func, FunctionTag _tag):actionID{-1}, tag{_tag}, action{_func} {}

IFunction::~IFunction(){}

FunctionTag IFunction::getTag(){
  return tag;
}

void IFunction::validate(int _id){
  actionID = _id;
}

bool IFunction::doFunction(){
  if(action != nullptr && actionID != -1 && tag != FunctionTag::NOTHING){
    action();
    return true;
  } else { 
    Logger::o << DECO << "IFunction is invalid (unvalidated by bus or null), or is NOTHING.\n";
    return false;
  }
}

int IFunction::getID(){
  return actionID ;
}

