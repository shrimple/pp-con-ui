#include "bus/Bus.h"

Bus::Bus():clientGen{0},iFuncGen{0},registeredClients{}{}

Bus::~Bus(){}

void Bus::bindToOperator(std::shared_ptr<BusOperator> _op, STR_ID _name, FunctionTag _tag){
    for(auto it = registeredClients.begin(); it != registeredClients.end(); it++ ){
        Logger::o << DECO << "Got '" << (*it)->getName() << "', looking for '" << _name << "'.\n" ; 

        if(*it != nullptr && (*it)->getName() == _name){
            Logger::o << DECO << "Binding operator to " << _name << "'s " << tag2str(_tag) << " function!\n";
            
            std::shared_ptr<BusSlave> slave = std::dynamic_pointer_cast<BusSlave>((*it));

            if(slave != nullptr){
                std::vector<std::shared_ptr<IFunction>> functionSet = slave->getFuncSet().getVector();

                for(auto it2 = functionSet.begin(); it2 != functionSet.end(); it2++ ){
                    if((*it2)->getTag() == _tag && (*it2)->getID() != -1){
                        Logger::o << DECO << "Bound successful...\n";
                        _op->bindTo((*it2));
                    }
                }

                return;
            }
        }
    }
}

void Bus::bindToOperator(std::shared_ptr<BusOperator> _op, int _cid, FunctionTag _tag){
    for(auto it = registeredClients.begin(); it != registeredClients.end(); it++ ){
        Logger::o << DECO << "Got '"<< (*it)->busClientId << "', looking for '" << _cid << "'.\n" ; 

        if((*it)->getID() == _cid){
            Logger::o << DECO << "Binding operator to " << (*it)->getName() << "'s " << tag2str(_tag) << " function!\n";

            std::shared_ptr<BusSlave> slave = std::dynamic_pointer_cast<BusSlave>(*it);

            if(slave != nullptr){
                std::vector<std::shared_ptr<IFunction>> functionSet = slave->getFuncSet().getVector();

                for(auto it2 = functionSet.begin(); it2 != functionSet.end(); it2++ ){
                    if((*it2)->getTag() == _tag && (*it2)->getID() != -1){
                        Logger::o << DECO << "Bound successful...\n";

                        _op->bindTo((*it2));
                    }
                }
            }
        }
    }
}

void Bus::registerOperator(BusOperator* _client){
    Logger::o << DECO << "registering client...\n";

    if(_client == nullptr){
        Logger::o << DECO << "Client is null...";
        return;
    }

    int newCID = getNewClientID();

    _client->registerMe(this, newCID);
    Logger::o << DECO << "Client of id'" << _client->busClientId << "', now registered within Bus vector!\n";

    return;
}

void Bus::registerSlave(bs_shp _slave){
    if(_slave == nullptr){
        Logger::o << DECO << "Slave is null...";
        return;
    }

    Logger::o << DECO << _slave->getFuncSet().getVector().size() << "1\n";

    if(_slave != nullptr){
        std::vector<std::shared_ptr<IFunction>> v = _slave->getFuncSet().getVector();
        
        for(auto it = v.begin(); it != v.end(); it++ ){
            (*it)->validate(getNewActionID());
        }

        Logger::o << DECO << "Client '" << _slave->getName() << "', IFuncs validated!\n";
    }

    registeredClients.push_back(_slave);
}

int Bus::getNewActionID(){
    return iFuncGen++;
}

int Bus::getNewClientID(){
    return clientGen++;
}
