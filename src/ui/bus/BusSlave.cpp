#include "bus/BusSlave.h"

BusSlave::BusSlave(std::vector<IFunction> _funcSet, std::string _name):BusClient(_name),slaveFunctions{_funcSet}{
    //Logger::o << DECO << "Constructed slave with " << slaveFunctions.size() << " IFuncs..\n";
} 
BusSlave::~BusSlave(){} 

void BusSlave::validate(BusRegPayload _p){
    if(_p.slaveReg != nullptr){
        Logger::o << DECO << "Before delegate.\n";
        _p.slaveReg(shared_from_this());
    }else if(_p.slaveReg == nullptr && busPtr == nullptr)
        Logger::o << DECO << "Client was not initialized via the bus. You must provide a valid reference to the bus validation cb!\n";
    else if(_p.slaveReg == nullptr && busPtr != nullptr)
        Logger::o << DECO << "Client" << busClientId << " already initialized.\n";
    
}

bool BusSlave::switchReceiveState(){
    return canReceiveAction = !canReceiveAction;
}

IFunctionSet BusSlave::getFuncSet(){
    return slaveFunctions;
}