#include "bus/IFunctionSet.h"

IFunctionSet::IFunctionSet(std::vector<IFunction> _actions){
    //Logger::o << DECO << "Received " << _actions.size() << " functions.\n";

    for(auto it = _actions.begin(); it != _actions.end(); it++){
        actions.push_back(std::make_shared<IFunction>(*it));
    }

    //Logger::o << DECO << "Loaded function set with " << actions.size() << " IFunctions.\n";
}

IFunctionSet::~IFunctionSet(){}

int IFunctionSet::size(){
    return actions.size();
}

std::vector<std::shared_ptr<IFunction>> IFunctionSet::getVector(){
    return actions; 
}

std::shared_ptr<IFunction> IFunctionSet::get(int _i){
    if(_i < actions.size()){
        auto it = actions.begin() + _i;
        return (*it);
    }else{
        Logger::o << DECO << "IFunction Index requested i: " << _i << ", is bigger than size of actions vector, " << actions.size() << "!\n";
        return nullptr;
    }
}

std::shared_ptr<IFunction> IFunctionSet::getByID(int _aid){
    for(auto it = actions.begin(); it != actions.end(); it++){
        if((*it)->getID() == _aid)
            return (*it);
    }

    Logger::o << DECO << "IFunction ID requested : " << _aid << ", was not found within ActionSet!\n";
    return nullptr;
}

std::shared_ptr<IFunction> IFunctionSet::getByTag(FunctionTag t){
    for(auto it = actions.begin(); it != actions.end(); it++){
        if((*it)->getTag() == t)
            return (*it);
    }

    Logger::o << DECO << "IFunction Tag requested : " << t << ", was not found within ActionSet!\n";
    return nullptr;
}