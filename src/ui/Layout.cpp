#include "Layout.h"
#include "bus/Bus.h"

Layout::Layout(const Layout& _l):BusSlave(_l){
	xOffset = _l.xOffset;
	yOffset = _l.yOffset;
	height = _l.height;
	width = _l.width;

	id = _l.id;

	lFlags.redraw = _l.lFlags.redraw;
	lFlags.wrapText = _l.lFlags.wrapText;
	lFlags.drawBorders = _l.lFlags.drawBorders;
	lFlags.focused = _l.lFlags.focused;
	lFlags.readyForDeletion = _l.lFlags.readyForDeletion;
	objVec = _l.objVec;
}

Layout::Layout(ID<UI> _id, int _w, int _h, int _x, int _y, std::string _name):
BusSlave(getIFunctions(), _name),
width{_w}, height{_h}, xOffset{_x}, yOffset{_y}, id{_id}, focusable{}, focused{focusable.begin()}, objVec{}{
	
	lFlags.redraw = true;
	lFlags.wrapText = false;
	lFlags.drawBorders = true;
	lFlags.focused = false;
	lFlags.readyForDeletion = false;
}

Layout::~Layout(){}

void Layout::removeObj(int _id){
	Logger::o << DECO << "removing obj of id '" << _id << "'...\n";
	for(auto it = objVec.begin(); it != objVec.end(); ++it){
		if((*it)->getObjID().getID() == _id){
			objVec.erase(it);
			lFlags.redraw = true;
		}
	}
}

void Layout::mvObj(int _x, int _y, int _id){
	Logger::o << DECO << "moving obj of id '" << _id << " to x:" << _x << ", y:" << _y << ", rel:" << "'...\n";

	for(auto it = objVec.begin(); it != objVec.end(); it++){
		if((*it)->getObjID().getID() == _id){
			lFlags.redraw = true;

			(*it)->setPosOffset(_x, _y);
		}
	}
}

//-1 = left, 1 = right
void Layout::mvObjFocus(int _dir){
	Logger::o << DECO << "moving focus " << _dir  << " to other " << focusable.size() << " selectable obj...\n";

	if(focusable.size() < 1 ){
		focused = focusable.end();
		return;
	}

	if((_dir == 1 && focused + 1 != focusable.end()) || (_dir == -1 && focused != focusable.begin())){
		(*focused)->setFocus(false); 
		focused += _dir; 
		(*focused)->setFocus(true);
		lFlags.redraw = true;
	}

}

std::shared_ptr<Obj> Layout::getFocused(){
	return *focused;
}

ID<UI> Layout::getObjID(){
	return id;
}

std::vector<std::shared_ptr<Obj>> Layout::getVector(){
	return objVec;
}

std::vector<std::shared_ptr<Obj>> Layout::getDrawData(){
	if(!isHidden()){
		lFlags.redraw = false;
		return objVec;
	}

	return {};
}

std::vector<IFunction> Layout::getIFunctions(){
	std::vector<IFunction> funcs;
	funcs.push_back(IFunction(std::bind(&Layout::readyToDelete, this), FunctionTag::KILL));
	funcs.push_back(IFunction(std::bind(&Layout::switchHideState, this), FunctionTag::HIDE));
	funcs.push_back(IFunction(std::bind(&Layout::switchCollapseState, this), FunctionTag::LAYOUT_COLLAPSE));
	return funcs;
}

int Layout::getHeight(){
	return height;
}

int Layout::getWidth(){
	return width;
}

int Layout::getYO(){
	return yOffset;
}

int Layout::getXO(){
	return xOffset;
}

int Layout::getSelectedIndex(){
	return std::distance(focusable.begin(), focused);
}

bool Layout::needsRedraw(){
	return lFlags.redraw;
}

bool Layout::isReadyToDelete(){
	return lFlags.readyForDeletion;
}

bool Layout::wrapsText(){
	return lFlags.wrapText;
}

bool Layout::hasBorders(){
	return lFlags.drawBorders;
}

bool Layout::isFocused(){
	return lFlags.focused;
}

bool Layout::isCollapsed(){
	return lFlags.collapsed;
}

bool Layout::isHidden(){
	return lFlags.hidden;
}

void Layout::setRedraw(bool _s){
	if(!isHidden()){
		lFlags.redraw = _s;
	} else {
		lFlags.redraw = false;
	}
}

void Layout::setHeight(int _h){
	if(_h > 0)
		height = _h;
}

void Layout::setWidth(int _w){
	if(_w > 0)
		width = _w;
}

void Layout::setPosOffset(int _x, int _y){
	if(_y > -1)
		yOffset = _y;

	if(_x > -1)
		xOffset = _x;
}

void Layout::readyToDelete(){
	lFlags.readyForDeletion = true;
	lFlags.redraw = true;
}

void Layout::setWrapText(bool _s){
	lFlags.wrapText = _s;
	lFlags.redraw = true;
}

void Layout::setDrawBorders(bool _s){
	lFlags.drawBorders = _s;
	lFlags.redraw = true;
}

void Layout::setFocus(bool _s){
	if(!isHidden()){
		lFlags.focused = _s;
		lFlags.redraw = true;
	}
}

void Layout::setCollapsed(bool _s){
	if(!isHidden()){
		lFlags.collapsed = _s;
		lFlags.redraw = true;
	}
}

void Layout::switchCollapseState(){
	if(lFlags.hidden)
		lFlags.collapsed = false;
	else
		lFlags.collapsed = true;

	lFlags.redraw = true;
	

	Logger::o << DECO << "Collapse state = " << b2str(lFlags.collapsed) << "\n";
}

void Layout::switchHideState(){
	if(lFlags.hidden)
		lFlags.hidden = false;
	else
		lFlags.hidden = true;
	
	//lFlags.hidden = !lFlags.hidden;
	lFlags.redraw = true;

	Logger::o << DECO << "Hide state = " << b2str(lFlags.hidden) << "\n";
}
