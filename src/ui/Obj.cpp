#include "Obj.h"

Obj::Obj(ID<Layout> _id, int _xOff, int _yOff, int _color, int _sColor, bool _selectable, std::string _data):
							  id{_id}, xOffset{_xOff}, yOffset{_yOff}, color{_color}, focusedColor{_sColor},focusable{_selectable}, data{_data}, focused{false}{}

Obj::Obj(const Obj& _obj): id{_obj.id}, xOffset{_obj.xOffset}, yOffset{_obj.yOffset}, color{_obj.color}, focusedColor{_obj.focusedColor},
								 data{_obj.data},focusable{_obj.focusable}, focused{_obj.focused}{}

Obj::~Obj(){
}

std::string Obj::getData(){
	return data;
}

int Obj::getColor(){
	return (focused) ? focusedColor : color;
}

void Obj::setColor(int _color){
	color = _color;
}

ID<Layout> Obj::getObjID(){
	return id;
}

void Obj::setPosOffset(int _xOff, int _yOff){
	if(_xOff > -1) xOffset += _xOff; 
	if(_yOff > -1) yOffset += _yOff;
}

void Obj::setFocusColor(int _col){
	focusedColor = _col;
}

void Obj::setFocusable(bool _state){
	focusable = _state;
}

void Obj::setFocus(bool _state){
	focused = _state;
}

int Obj::getXO(){
	return xOffset;
}

int Obj::getYO(){
	return yOffset;
}

int Obj::getFocusColor(){
	return focusedColor;
}

bool Obj::isFocusable(){
	return focusable;
}

bool Obj::isFocused(){
	return focused;
}