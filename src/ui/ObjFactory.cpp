#include "ObjFactory.h"

OFac::~OFac(){}

int OFac::objGen = 0;

std::shared_ptr<Layout> OFac::makeLayout(UI* _p, int _w, int _h, int _x, int _y, BusRegPayload _payload, STR_ID _name){
    ID<UI> id = ID<UI>(_p, objGen++);
    Logger::o << DECO << "id{ UI, id:" << id.getID() << "} x:" << _x << ", y:" << _y << ", w:" << _w << ", h:" << _h << ".\n";

    std::shared_ptr<Layout> layout = std::make_shared<Layout>(Layout(id, _w, _h, _x, _y, _name));
    layout->validate(_payload);

    return layout;
}

std::shared_ptr<ObjLabel> OFac::makeLabel(Layout* _p, int _x, int _y, std::string _str, int _c, BusRegPayload _payload, STR_ID _name){
    ID<Layout> id = ID<Layout>(_p, objGen++);
    Logger::o << DECO << "id{p:" << id.getParent()->getObjID().getID() << ", id:" << id.getID() << "} x:" << _x << ", y:" << _y << ", color:" << _c << ", datasize:" << _str.size() << ".\n";

    std::shared_ptr<ObjLabel> label = std::make_shared<ObjLabel>(ObjLabel(id, _x, _y, _c, _c, _str, _name));
    label->validate(_payload);
    
    return label;
}

std::shared_ptr<ObjButton> OFac::makeButton(Layout* _p, int _x, int _y, std::string _str, int _c, int _sC, BusRegPayload _payload, STR_ID _name){
    ID<Layout> id = ID<Layout>(_p, objGen++);
    Logger::o << DECO << "id{p:" << id.getParent()->getObjID().getID() << ", id:" << id.getID() << "} x:" << _x << ", y:" << _y << ", color:" << _c << ", datasize:" << _str.size() << ".\n";

    std::shared_ptr<ObjButton> button = std::make_shared<ObjButton>(ObjButton(id, _x, _y, _c, _sC, _str, nullptr, _name));
    button->validate(_payload);

    return button;
}