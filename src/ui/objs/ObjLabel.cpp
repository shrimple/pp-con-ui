#include "objs/ObjLabel.h"

ObjLabel::ObjLabel(ID<Layout> _id, int _xOff, int _yOff, int _color, int _selColor, std::string _data, STR_ID _name):
    Obj(_id, _xOff, _yOff, _color, _selColor, false, _data),
    BusSlave(getIFunctions(), _name){}

ObjLabel::~ObjLabel(){}

void ObjLabel::changeData(std::string _d){
    data = _d;
}

std::string ObjLabel::getData(){
    return data;
}

std::vector<IFunction> ObjLabel::getIFunctions(){
    return std::vector<IFunction>();
}
