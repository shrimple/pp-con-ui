#include "objs/ObjButton.h"

std::string ObjButton::getData(){
	return "*" + data;
}

ObjButton::ObjButton(ID<Layout> _id, int _xOff, int _yOff, int _color, int _selColor,  std::string _data, std::shared_ptr<IFunction> _f, STR_ID _name) :
	Obj(_id, _xOff, _yOff, _color, _selColor, true, _data),
	BusOperator(_f, _name){}

ObjButton::ObjButton(const ObjButton& _obj)
	:Obj(_obj.id, _obj.xOffset, _obj.yOffset, _obj.color, _obj.focusedColor, _obj.focusable, _obj.data),
	BusOperator(_obj){}

ObjButton::~ObjButton(){}




