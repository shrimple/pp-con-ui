#include "UI.h"

//index starts at 1
UI::UI(){
	layoutVec = {};
	bus = Bus();
	readyToClose = false;
	focusedLayout = layoutVec.begin();
	showConsole = false;

	initCurses();
	
	Logger::o << DECO << "Ui initialized...\n";
}

UI::~UI(){
}

void UI::initCurses(){
	initscr();
	nodelay(stdscr, true);
	Logger::init(90, 25, Position(0, 30));

	bool firstPass = true;

	if(has_colors() == FALSE){
		endwin();
		printf("NO COLOR SUPPORT, NO APP 4 U. >:)");
		exit(1);
	}

	start_color();
	initColorPairs();

	Logger::o << DECO << "Curses initialised successfully.\n";
}

//Initialize your color pairs here
//where param of init_pair( <id>, <FG/Text color>, <BG color> );
void UI::initColorPairs(){
	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	init_pair(4, COLOR_YELLOW, COLOR_BLACK);
	init_pair(5, COLOR_BLUE, COLOR_YELLOW);
	init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(7, COLOR_CYAN, COLOR_BLACK);
	init_pair(8, COLOR_BLACK, COLOR_WHITE);
}

void UI::switchLoggerViewState(){
	showConsole = !showConsole;
	if(showConsole)
		Logger::needDrawing = true;
	else
		update(true);

		
}

void UI::drawBorders(int _h, int _w, int _x, int _y){
	//Logger::o << DECO << "Drawing borders. w:"<< _w << ", h:" << _h << ", x:" << _x << ", y:" << _y <<".\n";
	
	attron(COLOR_PAIR(4));

	for(int i = _y; i < _h + _y + 1; i++){
		std::string border = "";

		if(_y - i == 0 || i / (_h + _y) == 1 )
			border = borderStr(_w, "#", '#'); 
		else
			border = borderStr(_w, "#", ' '); 

		mvprintw(i, _x, border.c_str());
	}

	attroff(COLOR_PAIR(4));
}

std::string UI::borderStr(int _width, std::string _border, char _fill){
	std::string border = "";

	if(_fill == '#')
		border = std::string(_width, _fill);
	else if(_fill == ' ')
		border += _border + std::string(_width-2, _fill) + _border;

	//Logger::o << DECO << "Got border '" << border << "'.\n";
	return border;
}

void UI::draw(){

	if(layoutVec.size() < 1){
		Logger::o << DECO << "Layout vector is empty!\n";
		return;
	}

	if(!showConsole){

		for(auto it = layoutVec.rbegin(); it != layoutVec.rend(); it++){
			//Logger::o << DECO << "Loop, Layout" << (*it)->getObjID().getID() << " redraw '" << (*it)->needsRedraw() << "'.\n";

			if((*it)->getObjID() != (*focusedLayout)->getObjID()){
				//Logger::o << DECO << "Drawing Layout" << (*it)->getObjID().getID() << "\n";
				printLayout((*it));
			}
		}

		//Logger::o << DECO << "printing fLayout" << focusedLayout->getObjID().getID() << ".\n";

		if(*focusedLayout != nullptr && focusedLayout != layoutVec.end()){
			printLayout(*focusedLayout);
		}

	}else{
		if(Logger::needDrawing)
			Logger::drawLogger();
	}


	refresh();
}

void UI::printLayout(std::shared_ptr<Layout> _layout){
	_layout->setRedraw(false);
	//Logger::o << DECO << "layout objVec size '" << _layout->getVector().size() << "'.\n";

	std::vector<std::shared_ptr<Obj>> v = _layout->getDrawData();

	if(_layout->hasBorders())
		drawBorders(_layout->getHeight(), _layout->getWidth(), _layout->getXO(), _layout->getYO());

	for(auto it = v.begin(); it != v.end(); it++){ 
		//Logger::o << DECO << "Printing obj of id '" << (*it)->getObjID().getID() << "'.\n";
		int color = (*it)->getColor();

		attron(COLOR_PAIR(color));
		mvprintw((*it)->getYO(), (*it)->getXO(), (*it)->getData().c_str());
		attroff(COLOR_PAIR(color));
	}
	

	//Logger::o << DECO << "Printed layout data of size " << _layout->getVector().size() << ".\n";
}

void UI::update(bool _force){
	if(checkForRedraw() || _force){
		clear();
		draw();
	}

	catchInput();
}

bool UI::checkForRedraw(){
	bool needs = false;

	if(Logger::needDrawing == true && showConsole == true){
		return true;
	}

	for(auto it = layoutVec.begin(); it != layoutVec.end(); it++){
		if((*it)->needsRedraw()){
			(*it)->setRedraw(false);
			needs = true;
		}
	}
	
	return needs;
}

std::shared_ptr<Layout> UI::loadLayout(Layout _layout, bool _focus){
	layoutVec.push_back(std::make_shared<Layout>(_layout));

	if(layoutVec.size() == 1 || _focus)
		focusedLayout = layoutVec.begin();
	
	Logger::o << DECO << "Loaded a new layout into UI vector of id '"<< layoutVec.back()->getObjID().getID() <<"'...\n";
	return layoutVec.back();
}

std::shared_ptr<Layout> UI::loadLayout(std::shared_ptr<Layout>_layout, bool _focus){
	layoutVec.push_back(std::move(_layout));

	if(layoutVec.size() == 1 || _focus)
		focusedLayout = layoutVec.begin();

	Logger::o << DECO << "Loaded a new layout into UI vector of id '"<< layoutVec.back()->getObjID().getID() <<"'...\n";
	return layoutVec.back();
}

BusRegPayload UI::getRegPayload(){
	BUS_REG cR = std::bind(&Bus::registerOperator, &bus, std::placeholders::_1);
	SLAVE_REG sR = std::bind(&Bus::registerSlave, &bus, std::placeholders::_1);
	
	BusRegPayload p = BusRegPayload(sR, cR);
	return p;
}

void UI::removeLayout(int _id){
	for(auto it = layoutVec.begin(); it != layoutVec.end(); ++it){
		if((*it)->getObjID().getID() == _id){
			layoutVec.erase(it);
		}
	}
}

Bus* UI::getBus(){
	return &bus;
}

std::shared_ptr<Layout> UI::getLayout(int _id){
	for(auto it = layoutVec.begin(); it != layoutVec.end(); ++it){
		if((*it)->getObjID().getID() == _id)
			return (*it);
	}

	Logger::o << DECO << "Found no obj with id of " << _id << "! Returning layoutVec.back()...\n";
	return layoutVec.back();
}

void UI::moveFocus(int _amt){
	if(layoutVec.size() < 1){
		focusedLayout = layoutVec.end();
		return;
	}

	if((_amt < 0 && focusedLayout != layoutVec.begin()) || (_amt > 0 && focusedLayout + 1 != layoutVec.end())){
		(*focusedLayout)->setFocus(false); 
		focusedLayout += _amt;
		(*focusedLayout)->setFocus(false);
	}
	
	return;
}

void UI::catchInput(){
	if(readyToClose)
		return;

	//int random = std::rand() % 8 + 1;
	int key = std::getchar();

	switch(key){
		// case 27:{
		// 	key = std::getchar();
		// 	Logger::o << DECO << "fkey"<< key << "\n";

		// 	if(key == 79){
		// 		key = std::getchar();
		// 		switch(key){
		// 			 //f2 i think?
		// 			case 80: switchLoggerViewState(); break;
		// 		}
		// 	}
		// }
		case 92:{ Logger::o << "{\\}\n"; switchLoggerViewState(); break; }

		case 119:{ 
			if(!showConsole){
				if(focusedLayout != layoutVec.end()){
 					(*focusedLayout)->mvObjFocus(-1); 
				}
			}else
				Logger::moveViewPort(-1);
		break; 
		}

		case 115:{
			if(!showConsole){
				if(focusedLayout != layoutVec.end())
				   	(*focusedLayout)->mvObjFocus(1); 
			}else
				Logger::moveViewPort(1);
		break; 
		}

		case 101:{ 
			Logger::o << DECO << "Are you sure you want to quit? e = yes.\n";
				if(std::getchar() == 101)
					exitUI();
			break;
		}
		case 99: {
			Logger::o << DECO << "switching from layout" << (*focusedLayout)->getObjID().getID() << ".\n";
			moveFocus(1);
			break;
		}

		case 120:{
			Logger::o << DECO << "switching from layout" << (*focusedLayout)->getObjID().getID() << ".\n";
			moveFocus(-1);
			break;
		}

		case 102:{ 
			Logger::o  << "{f}\n"; 
			std::shared_ptr<ObjButton> but = std::dynamic_pointer_cast<ObjButton>((*focusedLayout)->getFocused());

			if(but == nullptr)
				return;
			
			std::shared_ptr<BusOperator> op = std::dynamic_pointer_cast<BusOperator>(but);

			if(op != nullptr){
				op->doBound();
			}

			break; 
		}
		default:{ Logger::o << "{" << key << "}\n"; break; }
	}
}

bool UI::isReadyToClose(){
	return readyToClose;
}

void UI::exitUI(){
	readyToClose = true;
}